import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AllSubstitutePage} from "../all-substitute/all-substitute";
import {MessagesPage} from "../messages/messages";
import {UserJobsPage} from "../user-jobs/user-jobs";

@Component({
    selector: 'page-tabs',
    template: `<ion-tabs>
                    <ion-tab [root]="tab1Root" tabTitle="מחליפות"></ion-tab>
                    <ion-tab [root]="tab2Root" tabTitle="המשרות שלי"></ion-tab>
                    <ion-tab [root]="tab3Root" tabTitle="הודעות" tabIcon="star"></ion-tab>
                  </ion-tabs>`
})
export class TabsPage {
    tab1Root = AllSubstitutePage;
    tab2Root = UserJobsPage;
    tab3Root = MessagesPage;

    constructor(public navCtrl: NavController) {

    }

}
