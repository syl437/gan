import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserJobsPage } from './user-jobs';

@NgModule({
  declarations: [
    UserJobsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserJobsPage),
  ],
})
export class UserJobsPageModule {}
