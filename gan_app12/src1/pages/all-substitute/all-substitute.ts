import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {substituteService} from "../../services/substituteService";
import {Config} from "../../services/config";

/**
 * Generated class for the AllSubstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-substitute',
  templateUrl: 'all-substitute.html',
})
export class AllSubstitutePage {

  public Substitute:any[] = [];

  constructor(public navCtrl: NavController, public navParams:NavParams, public substitute:substituteService, public Settings:Config) {
      this.substitute.getAllSubstitute('getAllSubstitute').then((data: any) => {
          this.Substitute = data;
          Settings.Substitute = data.length;
          console.log("Jobs : " , this.Substitute)
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllSubstitutePage');
  }

}
