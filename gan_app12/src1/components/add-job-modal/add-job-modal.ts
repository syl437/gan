import {Component} from '@angular/core';
import {ViewController} from "ionic-angular";
import {DatePicker} from "@ionic-native/date-picker";

/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
@Component({
    selector: 'add-job-modal',
    templateUrl: 'add-job-modal.html'
})

export class AddJobModalComponent {
    
    constructor(public viewCtrl: ViewController) {
    
    }
    
    onSubmit(form)
    {
        let data = {'date': form.value.date , 'time' : form.value.time , 'info' : form.value.info};
        this.viewCtrl.dismiss(data);
    }
}
