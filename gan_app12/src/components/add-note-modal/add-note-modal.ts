import {Component} from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";
import {DatePicker} from "@ionic-native/date-picker";
import {toastService} from "../../services/toastService";

/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
@Component({
    selector: 'add-job-modal',
    templateUrl: 'add-note-modal.html'
})

export class AddNoteModalComponent {
    
    public info;
    public title;
    
    constructor(public viewCtrl: ViewController,private navParams: NavParams , public Toast:toastService) {
    
    }
    
    onSubmit(form ,type)
    {
        if(type == 1)
        {
            if(form.value.title == undefined)
                this.Toast.presentToast('חובה לבחור נושא');
            else if(form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור תבנית');
            else
            {
                let data = {'title': form.value.title  , 'info' : form.value.info ,'type':type};
                this.viewCtrl.dismiss(data);
            }
        }
        else
        {
            let data = {'title': form.value.title  , 'info' : form.value.info ,'type':type};
            this.viewCtrl.dismiss(data);
        }
    }



    ionViewWillEnter() {
        this.info = this.navParams.get('info');
        this.title = this.navParams.get('title');
        console.log("TT : " , this.info + " : " + this.title)
    }
}
