import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenJobsPage } from './open-jobs';

@NgModule({
  declarations: [
    OpenJobsPage,
  ],
  imports: [
    IonicPageModule.forChild(OpenJobsPage),
  ],
})
export class OpenJobsPageModule {}
