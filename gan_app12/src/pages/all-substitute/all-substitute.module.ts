import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllSubstitutePage } from './all-substitute';

@NgModule({
  declarations: [
    AllSubstitutePage,
  ],
  imports: [
    IonicPageModule.forChild(AllSubstitutePage),
  ],
})
export class AllSubstitutePageModule {}
