import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AddJobModalComponent} from "../../components/add-job-modal/add-job-modal";
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";
import {ProjectBidsPage} from "../project-bids/project-bids";
import { App } from 'ionic-angular';
import {sent_to_server_service} from "../../services/sent_to_server_service";

/**
 * Generated class for the UserJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-user-jobs',
    templateUrl: 'user-jobs.html',
})
export class UserJobsPage {

    public jobs:any[] = [];

    constructor(private app: App , public navCtrl: NavController, public navParams: NavParams , public modalCtrl: ModalController, public jobsService:JobsService , public Settings:Config, public serverService:sent_to_server_service) {
        this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
            this.jobs = data;
            Settings.JobsCount = data.length;
            console.log("Jobs12 : " , this.jobs)
        });
        console.log('UserJobsPage');
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad UserJobsPage');
    }

    openModal() {
        let jobsModal = this.modalCtrl.create(AddJobModalComponent);
        jobsModal.present();

        jobsModal.onDidDismiss(data => {
            console.log("jb : " , data);
            if(data.type == 1)
            {
                this.jobsService.addJob('addJob',data).then((data: any) => {
                    this.jobs = data;
                });
            }
        });
    }

    updateModal(i) {
        let noteModal = this.modalCtrl.create(AddJobModalComponent, {
            job:this.jobs[i]
        });

        noteModal.present();

        noteModal.onDidDismiss(data => {
            console.log(data);
             this.serverService.updateJob('updateJob', data,this.jobs[i]['id']).then((data: any) => {
                 this.jobs = data;
             });
        });
    }


    open_close_project(job)
    {
        job.is_close == 0 ?  job.is_close = 1 :  job.is_close = 0;

        this.jobsService.UpdateJobState('UpdateJobState',job.id,job.is_close).then((data: any) => {
            console.log("JobState : " , data)
        });
    }

    checkState(type)
    {
        let str = (type == 0) ? 'הפרוייקט פתוח' : 'הפרוייקט סגור';
        return str;
    }

    open_project_bid(i)
    {
        this.app.getRootNav().setRoot(ProjectBidsPage,{project:this.jobs[i].bids});
        //this.navCtrl.push(ProjectBidsPage,{project:this.jobs[i].bids});
    }

}
