import {Component} from '@angular/core';
import {
    ActionSheetController, IonicPage, Loading, LoadingController, NavController, NavParams, Platform,
    ToastController
} from 'ionic-angular';
import {loginService} from "../../services/loginService";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import {toastService} from "../../services/toastService";
import {UserJobsPage} from "../user-jobs/user-jobs";
import {TabsPage} from "../tabs/tabs";
import {HomePage} from "../home/home";
import {LoginPage} from "../login/login";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {Diagnostic} from "@ionic-native/diagnostic";
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

declare let cordova: any;

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    public info: Object = {
        name: '',
        phone: '',
        address: '',
        info: '',
        password: '',
        mail: '',
        image: '',
        type: '',
        area: ''
    };

    public Regions: any[] = [];
    public Types: any[] = [];
    public emailregex;
    public base64Image: string;
    lastImage: string = null;
    loading: Loading;
    correctPath;
    currentName;
    public Type:Number;

    public options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    }

    constructor(public navCtrl: NavController, public navParams: NavParams, public Login: loginService, public Toast: toastService, private camera: Camera, private diagnostic: Diagnostic, private transfer: FileTransfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController) {
        this.Login.getRegions('GetRegions').then(
            (data: any) => {
                console.log("Regions : ", data);
                this.Regions = data;
            });

        this.Login.GetTypes('GetTypes').then(
            (data: any) => {
                console.log("Types : ", data);
                this.Types = data;
            });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    doRegister() {
        console.log("register response1: ");
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא')
        else if (this.info['phone'].length < 8)
            this.Toast.presentToast('הכנס מספר טלפון חוקי')
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין')
        }
        else if (this.info['address'].length < 2)
            this.Toast.presentToast('הכנס כתובת חוקית')
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה')
        else if (this.info['area'] == '')
            this.Toast.presentToast('בחר איזור')
        else if (this.info['type'] == '')
            this.Toast.presentToast('בחר סוג תפקיד')
        else {
            this.Login.RegisterUser("RegisterUser", this.info).then(data => {
                if (data == 0) {
                    this.Toast.presentToast('ארעה שגיאה , אנא נסה שנית');
                }
                else {
                    window.localStorage.id = data;
                    this.navCtrl.push(HomePage);
                }
            });
        }
    }

    GoToLogin() {
        this.navCtrl.push(LoginPage);
    }


    /// Image Upload


    public presentActionSheet() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    }



    public takePicture(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };

        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            // Special handling for Android library
            console.log("f0");
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                console.log("f1");
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        this.correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(this.correctPath, this.currentName, this.createFileName());
                    });
            } else {
                console.log("f1");
                this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                console.log("f2 : " , this.currentName);
                this.correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                console.log("f3 : " , this.correctPath);
                this.copyFileToLocalDir(this.correctPath, this.currentName, this.createFileName());
                console.log("f4");
            }
        }, (err) => {
            this.presentToast('Error while selecting image.');
        });
    }


    // Create a new name for the image
    private createFileName() {
        console.log("f1");
        var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";
        return newFileName;
    }

// Copy the image to a local folder
    private copyFileToLocalDir(namePath, currentName, newFileName) {
        console.log("f2 : "+ namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.lastImage = newFileName;
            this.uploadImage();
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }

    public presentToast(text) {
        console.log("f3");
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    }

// Always get the accurate path to your apps folder
    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }


    public uploadImage() {
        // Destination URL
        console.log("Up1  : ")
        var url = "http://www.tapper.co.il/gan/laravel/public/api/GetFile";
        // File for Upload
        console.log("Up2  : " + this.lastImage + " : " + this.pathForImage(this.lastImage));
        var targetPath = this.pathForImage(this.lastImage);

        // File name only
        var filename = this.lastImage;

        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params : {'fileName': filename}
        };

        const fileTransfer: FileTransferObject = this.transfer.create();

        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();

        // Use the FileTransfer to upload the image
        console.log("Up1  : " , targetPath + " : " + url + " : " + options);

        fileTransfer.upload(targetPath, url, options).then(data => {
            console.log("Updata  : " , data.response );
            this.loading.dismissAll();
        }, err => {
            this.loading.dismissAll()
            this.presentToast('Error while uploading file.');
        });
    }
}
