import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AddJobModalComponent} from "../../components/add-job-modal/add-job-modal";
import {AddNoteModalComponent} from "../../components/add-note-modal/add-note-modal";
import {sent_to_server_service} from "../../services/sent_to_server_service";

/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-notes',
    templateUrl: 'notes.html',
})

export class NotesPage {
    
    public Notes:any[]=[];
    
    constructor(public navCtrl: NavController, public navParams: NavParams , public modalCtrl: ModalController, public serverService:sent_to_server_service) {
        this.getAllNotes();
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad NotesPage');
    }
    
    openModal() {
        let noteModal = this.modalCtrl.create(AddNoteModalComponent);
        
        noteModal.present();
    
        noteModal.onDidDismiss(data => {
            if (data.type == 1)
            {
                this.serverService.addNote('addNote', data).then((data: any) => {
                    this.Notes = data;
                });
            }
        });
    }
    
    updateModal(i) {
        let noteModal = this.modalCtrl.create(AddNoteModalComponent, {
            title:this.Notes[i]['title'],
            info:this.Notes[i]['info']
        });
        
        noteModal.present();
        
        noteModal.onDidDismiss(data => {
             this.serverService.updateNote('updateNote', data ,this.Notes[i]['id']).then((data: any) => {
                 this.Notes = data;
             });
        });
    }
    
    getAllNotes()
    {
        this.serverService.getAllNotes('getAllNotes').then((data: any) => {
            this.Notes = data;
        });
    }
    
    
    deleteNote(id)
    {
        this.serverService.deleteNote('deleteNote', id).then((data: any) => {
            this.Notes = data;
        });
    }
    
}
