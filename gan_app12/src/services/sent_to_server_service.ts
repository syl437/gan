import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";

@Injectable()

export class sent_to_server_service
{
    public ServerUrl;
    
    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.ServerUrl;
    };
    
    addNote(url:string , info:Object)
    {
        let body = new FormData();
        body.append('title',info["title"]);
        body.append('info',info["info"]);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("titleInfo : " , data)}).toPromise();
    }
    
    updateNote(url:string , info:Object , id)
    {
        let body = new FormData();
        body.append('title',info["title"]);
        body.append('info',info["info"]);
        body.append('id',id);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("titleInfo : " , data)}).toPromise();
    }
    
    getAllNotes(url:string)
    {
        let body = new FormData();
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }
    
    deleteNote(url:string,id)
    {
        let body = new FormData();
        body.append('id',id);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }


    //Bid

    addBid(url:string,job,note)
    {
        let body = new FormData();
        body.append('job',job);
        body.append('note',note);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }


    getHomeDetails(url:string)
    {
        let body = new FormData();
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    updateJob(url:string , info:Object , id)
    {
        let body = new FormData();
        body.append('Date2',info['date']);
        body.append('Time2',info['time']);
        body.append('info',info['info']);
        body.append('id',id);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("titleInfo : " , data)}).toPromise();
    }
};


