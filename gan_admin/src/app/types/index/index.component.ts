import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {
    
    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host;
    settings = '';
    Id = '';
    avatar = '';
    public folderName: string = 'types';
    public addButton: string = 'הוסף סוג הרשאה'
    
    
    constructor(public MainService: MainService, private route: ActivatedRoute, settings: SettingsService) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            console.log("Catch : ", this.Id)
            this.host = settings.host, this.avatar = settings.avatar,
                this.GetTypes();
        });
    }
    
    GetTypes() {
        this.MainService.GetItems('GetTypes').then((data: any) => {
            this.ItemsArray = data;
            this.ItemsArray1 = data;
            console.log(this.ItemsArray[0])
        })
    }
    
    ngOnInit() {
    }
    
    DeleteItem(i) {
        this.MainService.DeleteItem('DeleteType', this.ItemsArray[i].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })
    }
    
    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }
    
    goToCarsPage(i) {
    
    }
    
}
